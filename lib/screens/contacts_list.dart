import 'package:flutter/material.dart';
import 'package:lesson_10/models/contatto.dart';
import 'package:lesson_10/shared/contact_card.dart';
import 'package:provider/provider.dart';

class ContactsList extends StatefulWidget {
  @override
  _ContactsListState createState() => _ContactsListState();
}

class _ContactsListState extends State<ContactsList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(16, 0, 16, 0),
      child: Consumer<ContactsModel>(builder: (context, data, child) {
        return ListView.builder(
          itemCount: data.numberOfElements,
          itemBuilder: (context, index) {
            return ContactCard(
              contatto: data.items[index],
              action: () {
                Navigator.of(context).pushNamed(
                  '/dettaglio',
                  arguments: data.items[index],
                );
              },
            );
          },
        );
      }),
    );
  }
}
