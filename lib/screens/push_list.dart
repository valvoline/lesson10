import 'package:flutter/material.dart';

class PushNotificationList extends StatefulWidget {
  @override
  _PushNotificationListState createState() => _PushNotificationListState();
}

class _PushNotificationListState extends State<PushNotificationList> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[50],
      appBar: AppBar(
        leading: SizedBox(),
        title: Text('Push Notifications'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.close),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ],
      ),
      body: Container(
          padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: ListView(
            children: <Widget>[
              Card(
                child: ListTile(
                  leading: Icon(Icons.cloud_done),
                  title: Text('Salvataggio offline'),
                  subtitle: Text('Completato alle ore 18:30 del 17/12/2019'),
                ),
              ),
              Card(
                child: ListTile(
                  leading: Icon(Icons.check),
                  title: Text('Nuovi voti per tuo figlio'),
                  subtitle: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            'Italiano: ',
                            style: TextStyle(
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                          Text('4 1/2'),
                          SizedBox(
                            width: 8,
                          ),
                          Container(
                            padding: EdgeInsets.all(3),
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 1,
                          ),
                          Container(
                            padding: EdgeInsets.all(3),
                            color: Colors.red,
                          ),
                          SizedBox(
                            width: 1,
                          ),
                          Container(
                            padding: EdgeInsets.all(3),
                            color: Colors.orange,
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }
}
