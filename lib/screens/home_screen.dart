// import 'package:device_token_interceptor/device_token_interceptor.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:lesson_10/screens/contact_details.dart';
import 'package:lesson_10/screens/contacts_list.dart';
import 'dart:io';
import 'package:lesson_10/screens/news_list.dart';
import 'package:lesson_10/screens/push_list.dart';
import 'package:lesson_10/shared/utilities.dart';
import 'package:badges/badges.dart';
import 'package:provider/provider.dart';
import 'package:lesson_10/models/user.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with WidgetsBindingObserver {
  int _tabBarCurrentIndex = 0;
  int _badgeCounter = 0;

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  // DeviceTokenInterceptor _tokenInterceptor = DeviceTokenInterceptor();

  void setupPushNotifications() {
    // _tokenInterceptor.onTokenRefresh.listen((data) {
    //   print("apn token: $data");
    // });
    if (Platform.isIOS) {
      _firebaseMessaging.requestNotificationPermissions(
          IosNotificationSettings(sound: true, badge: true, alert: true));
      _firebaseMessaging.onIosSettingsRegistered
          .listen((IosNotificationSettings settings) {
        print("Settings registered: $settings");
      });
    }

    _firebaseMessaging.getToken().then((token) {
      print("firebaseToken: $token");
    });

    _firebaseMessaging.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('on message $message');
        setState(() {
          _badgeCounter++;
        });
      },
      onResume: (Map<String, dynamic> message) async {
        print('on resume $message');
      },
      onLaunch: (Map<String, dynamic> message) async {
        print('on launch $message');
      },
      onBackgroundMessage: null,
    );
  }

  @override
  void initState() {
    super.initState();
    setupPushNotifications();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      print("resumed");
    }
  }

  final GlobalKey<NavigatorState> navigatorKey1 =
      new GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[50],
      appBar: AppBar(
        title: Text('ArgoNext'),
        centerTitle: true,
        backgroundColor: Colors.blue,
        actions: <Widget>[
          Badge(
            showBadge: _badgeCounter > 0,
            position: BadgePosition.topRight(top: 0, right: 3),
            badgeContent: Text(
              '$_badgeCounter',
              style: TextStyle(color: Colors.white),
            ),
            child: IconButton(
              icon: Icon(Icons.message),
              onPressed: () {
                Navigator.of(context).push(
                  MaterialPageRoute(
                    fullscreenDialog: true,
                    builder: (BuildContext context) {
                      return PushNotificationList();
                    },
                  ),
                );
              },
            ),
          ),
        ],
      ),
      drawer: Consumer<UserModel>(
        builder: (context, data, child) {
          return SizedDrawer(
            headerTitle: 'Impostazioni',
            headerBackgroundColor: Colors.orange[700],
            children: <Widget>[
              Card(
                child: ListTile(
                  onTap: () {
                    Navigator.of(context).pop();
                    data.logout();
                  },
                  leading: Icon(Icons.exit_to_app),
                  title: Text('Logout'),
                ),
              )
            ],
          );
        },
      ),
      body: BodyContainer(
        tabIndex: _tabBarCurrentIndex,
        children: <Widget>[
          NavigatorWrapper(
            routes: {
              '/': (context) {
                return NewsList();
              },
              '/details': (context) {
                return Container(
                  child: Center(
                    child: RaisedButton(
                      child: Text('back'),
                      color: Colors.indigo,
                      onPressed: () {
                        Navigator.pop(context);
                      },
                    ),
                  ),
                );
              },
            },
          ),
          NavigatorWrapper(
            routes: {
              '/': (context) => ContactsList(),
              '/dettaglio': (context) => DettaglioContatto()
            },
          ),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        elevation: 2,
        backgroundColor: Colors.grey[100],
        currentIndex: _tabBarCurrentIndex,
        onTap: (index) {
          setState(() => _tabBarCurrentIndex = index);
          print("tapped $index");
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.rss_feed),
            title: Text('Novità'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.people),
            title: Text('Docenti'),
          )
        ],
      ),
    );
  }
}
