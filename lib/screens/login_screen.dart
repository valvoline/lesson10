import 'package:flutter/material.dart';
import 'package:lesson_10/models/user.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:provider/provider.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String _tmpUsername = "";
  String _tmpPassword = "";
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.brown[50],
      appBar: AppBar(
        backgroundColor: Colors.blue,
        title: SizedBox(
            height: 40,
            child: Image.network('https://www.argosoft.it/area-img/logo.png')),
      ),
      body: Consumer<UserModel>(
        builder: (context, data, child) {
          return Container(
            child: Stack(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(18.0),
                  child: Center(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          TextFormField(
                            onChanged: (val) => _tmpUsername = val,
                            validator: (value) {
                              if (value.length < 4) {
                                return 'Nome utente non valido.';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              icon: Icon(Icons.person),
                              labelText: "nome utente",
                              labelStyle: TextStyle(color: Colors.grey[700]),
                              filled: true,
                              fillColor: Colors.brown[50],
                              focusColor: Colors.white30,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey[700],
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          TextFormField(
                            obscureText: true,
                            onChanged: (val) => _tmpPassword = val,
                            validator: (value) {
                              if (value.length < 4) {
                                return 'Password non valida.';
                              }
                              return null;
                            },
                            decoration: InputDecoration(
                              icon: Icon(Icons.vpn_key),
                              labelText: "password",
                              labelStyle: TextStyle(color: Colors.grey[700]),
                              filled: true,
                              fillColor: Colors.brown[50],
                              focusColor: Colors.white30,
                              border: OutlineInputBorder(
                                borderSide: BorderSide(
                                    color: Colors.grey[200],
                                    width: 1.0,
                                    style: BorderStyle.solid),
                              ),
                            ),
                          ),
                          SizedBox(height: 20),
                          RaisedButton(
                            padding: EdgeInsets.fromLTRB(60, 16, 60, 16),
                            onPressed: () async {
                              if (_formKey.currentState.validate()) {
                                setState(() => _isLoading = true);
                                User response = await UserModel.login(
                                    _tmpUsername, _tmpPassword);
                                setState(() => _isLoading = false);
                                if (response == null) {
                                  showDialog(
                                    barrierDismissible: false,
                                    context: context,
                                    builder: (context) {
                                      return AlertDialog(
                                        title: Text("Errore login"),
                                        content: Text(
                                            "Nome utente o password errati."),
                                        actions: <Widget>[
                                          FlatButton(
                                            child: Text('Ok'),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                          )
                                        ],
                                      );
                                    },
                                  );
                                } else {
                                  data.storeCredentials(response);
                                }
                              }
                            },
                            child: Text('Login',
                                style: TextStyle(color: Colors.white)),
                            color: Colors.blue,
                            disabledColor: Colors.grey[500],
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                _isLoading
                    ? SizedBox.expand(
                        child: Container(
                          decoration: BoxDecoration(color: Colors.black26),
                          child: SpinKitCircle(
                            color: Colors.white,
                            size: 50.0,
                          ),
                        ),
                      )
                    : SizedBox(),
              ],
            ),
          );
        },
      ),
    );
  }
}
