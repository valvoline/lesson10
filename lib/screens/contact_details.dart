import 'package:flutter/material.dart';
import 'package:lesson_10/models/contatto.dart';
import 'package:provider/provider.dart';

class DettaglioContatto extends StatefulWidget {
  @override
  _DettaglioContattoState createState() => _DettaglioContattoState();
}

class _DettaglioContattoState extends State<DettaglioContatto> {
  int numeroAssenze = 0;

  Contatto data;

  @override
  Widget build(BuildContext context) {
    data = data == null ? ModalRoute.of(context).settings.arguments : data;

    numeroAssenze = (data != null && data.assenze != null) ? data.assenze : 0;

    return Consumer<ContactsModel>(
      builder: (context, dataModel, child) {
        return Scaffold(
          backgroundColor: Colors.brown[50],
          body: Container(
            margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    color: Colors.grey[800],
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  Center(
                    child: CircleAvatar(
                      backgroundImage: NetworkImage('${data.thumb}'),
                      radius: 48.0,
                    ),
                  ),
                  Divider(
                    height: 60.0,
                    color: Colors.grey[500],
                  ),
                  Text(
                    'NOME',
                    style: TextStyle(
                      color: Colors.grey,
                      letterSpacing: 2.0,
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    '${data.nome}',
                    style: TextStyle(
                      color: Colors.grey[800],
                      letterSpacing: 1.0,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    'SCUOLA DI APPARTENENZA',
                    style: TextStyle(
                      color: Colors.grey,
                      letterSpacing: 2.0,
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    '${data.scuola}',
                    style: TextStyle(
                      color: Colors.grey[800],
                      letterSpacing: 1.0,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(
                    height: 24.0,
                  ),
                  Text(
                    'NUMERO DI ASSENZE',
                    style: TextStyle(
                      color: Colors.grey,
                      letterSpacing: 2.0,
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    '$numeroAssenze',
                    style: TextStyle(
                      color: Colors.grey[800],
                      letterSpacing: 1.0,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Divider(
                    height: 60.0,
                    color: Colors.grey[500],
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.email,
                        size: 24,
                        color: Colors.grey[400],
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      Text(
                        '${data.scuola}',
                        style: TextStyle(
                          color: Colors.grey[400],
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.phone,
                        size: 24,
                        color: Colors.grey[400],
                      ),
                      SizedBox(
                        width: 8.0,
                      ),
                      Text(
                        '${data.telefono}',
                        style: TextStyle(
                          color: Colors.grey[400],
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
            backgroundColor: Colors.blue,
            onPressed: () {
              setState(() {
                numeroAssenze += 1;
                var newData = data;
                newData.assenze = numeroAssenze;
                dataModel.update(data, newData);
              });
            },
            child: Icon(
              Icons.plus_one,
            ),
          ),
        );
      },
    );
  }
}
