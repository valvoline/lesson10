import 'package:flutter/material.dart';
import 'package:lesson_10/models/news.dart';

class NewsList extends StatefulWidget {
  @override
  _NewsListState createState() => _NewsListState();
}

class _NewsListState extends State<NewsList> {
  List<News> items = [];
  @override
  void initState() {
    super.initState();
    _fetcher();
  }

  Future<void> _fetcher() async {
    var retObject = await NewsModel.fetchNews();
    if (retObject != null) {
      setState(() {
        items = retObject;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(0),
      child: items.length > 0
          ? RefreshIndicator(
              onRefresh: _fetcher,
              child: ListView.builder(
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return Card(
                    child: ListTile(
                      leading: Icon(
                        items[index].icona,
                        color: Colors.grey[800],
                      ),
                      title: Text('${items[index].titolo}'),
                      subtitle: Text('${items[index].descrizione}'),
                    ),
                  );
                },
              ),
            )
          : Center(
              child: Text(
                'Non ci sono news!',
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w300,
                    letterSpacing: 1),
              ),
            ),
    );
  }
}
