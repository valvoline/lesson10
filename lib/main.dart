import 'package:flutter/material.dart';
import 'package:lesson_10/models/user.dart';
import 'package:lesson_10/models/contatto.dart';
import 'package:lesson_10/screens/home_screen.dart';
import 'package:lesson_10/screens/login_screen.dart';
import 'package:provider/provider.dart';

void main() => runApp(CaptiveWrapper());

class CaptiveWrapper extends StatefulWidget {
  @override
  _CaptiveWrapperState createState() => _CaptiveWrapperState();
}

class _CaptiveWrapperState extends State<CaptiveWrapper> {
  @override
  Widget build(BuildContext context) {
    // Provider(create: (_) => UserModel(), child: ...)
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (context) => UserModel(),
        ),
        ChangeNotifierProvider(
          create: (context) => ContactsModel(),
        ),
      ],
      child: Consumer<UserModel>(
        builder: (context, data, child) {
          return MaterialApp(
              debugShowCheckedModeBanner: false,
              home: data.hasValidCredentials() ? HomeScreen() : LoginScreen());
        },
      ),
    );
  }
}
