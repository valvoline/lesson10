import 'dart:convert';
import 'package:flutter/material.dart';
import 'dart:collection';
import 'package:http/http.dart';

class Contatto {
  String nome;
  String thumb;
  String scuola;
  String telefono;
  String email;
  int assenze;
  Contatto({this.nome, this.thumb, this.scuola, this.telefono, this.email});
}

class ContactsModel extends ChangeNotifier {
  final List<Contatto> _items = [];

  UnmodifiableListView<Contatto> get items => UnmodifiableListView(_items);
  int get numberOfElements => _items.length;

  void fetchData() async {
    try {
      Response response = await get(
          'https://my-json-server.typicode.com/valvoline/demo/contatti');
      var json = jsonDecode(response.body);

      if (json is List<dynamic>) {
        //be sure that this is a list
        for (final a in json) {
          this.add(Contatto(
              email: a["email"],
              nome: a["nome"],
              thumb: a["thumb"],
              telefono: a["telefono"],
              scuola: a["scuola"]));
        }
      } else {
        throw Exception('json format error');
      }
    } catch (e) {
      print(e);
    }
  }

  ContactsModel() {
    fetchData();
  }

  void add(Contatto contatto) {
    _items.add(contatto);
    notifyListeners();
  }

  void remove(Contatto contatto) {
    _items.remove(contatto);
    notifyListeners();
  }

  void update(Contatto oldVersion, Contatto newVersion) {
    //get index of element to replace
    var indexOf = _items.indexOf(oldVersion);

    //replace element
    _items.replaceRange(indexOf, indexOf + 1, [newVersion]);
  }
}
