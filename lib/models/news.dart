import 'dart:convert';
import 'package:http/http.dart';
import 'package:flutter/material.dart';

class News {
  String titolo;
  String descrizione;
  IconData icona;

  News({this.titolo, this.descrizione, this.icona});
}

class NewsModel {
  static IconData _integerToIcon(int anIndex) {
    switch (anIndex) {
      case 0:
        return Icons.warning;
      case 1:
        return Icons.info;
      case 2:
        return Icons.insert_comment;
      default:
        return Icons.grain;
    }
  }

  static Future<dynamic> fetchNews() async {
    List<News> retObject = [];
    try {
      Response response =
          await get('https://my-json-server.typicode.com/valvoline/demo/news');
      var json = jsonDecode(response.body);
      if (json is List<dynamic>) {
        for (final aNews in json) {
          retObject.add(
            News(
              titolo: aNews["titolo"],
              descrizione: aNews["descrizione"],
              icona: _integerToIcon(aNews["icona"]),
            ),
          );
        }
        return retObject;
      }
    } catch (e) {
      print('[ERROR] - fetchNews: $e');
      return null;
    }
  }
}
