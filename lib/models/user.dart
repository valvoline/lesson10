import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart';

class User {
  String username;
  String password;
  int level;
  String token;

  User({this.username, this.password, this.level, this.token});
}

class UserModel extends ChangeNotifier {
  User _user = User();

  void storeCredentials(User newUser) {
    _user = newUser;
    notifyListeners();
  }

  bool hasValidCredentials() {
    return (_user.token != null);
  }

  void logout() {
    _user = User();
    notifyListeners();
  }

  Future<dynamic> sharedLogin(String username, String password) async {
    User retUser;
    try {
      Response response =
          await get('https://my-json-server.typicode.com/valvoline/demo/login');
      var json = jsonDecode(response.body);
      if (json is List<dynamic>) {
        for (final anUser in json) {
          if (anUser["username"] == username &&
              anUser["password"] == password) {
            retUser = User(
                username: username,
                password: password,
                level: anUser["level"],
                token: anUser["token"]);
            return retUser;
          }
        }
      }
      return null;
    } catch (e) {
      print('[ERROR] - login: $e');
      return null;
    }
  }

  static Future<dynamic> login(String username, String password) async {
    User retUser;
    try {
      Response response =
          await get('https://my-json-server.typicode.com/valvoline/demo/login');
      var json = jsonDecode(response.body);
      if (json is List<dynamic>) {
        for (final anUser in json) {
          if (anUser["username"] == username &&
              anUser["password"] == password) {
            retUser = User(
                username: username,
                password: password,
                level: anUser["level"],
                token: anUser["token"]);
            return retUser;
          }
        }
      }
      return null;
    } catch (e) {
      print('[ERROR] - login: $e');
      return null;
    }
  }
}
