import 'package:flutter/material.dart';

/* 
  Return a Widget that wrap the Body within a Stack using the Offstage Widget.
  This way you can switch TabBar easily without loosing state
*/
class BodyContainer extends StatelessWidget {
  final int tabIndex;
  final List<Widget> children;

  const BodyContainer({Key key, this.tabIndex, this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
        children: this
            .children
            .asMap()
            .map((iteratorIndex, aChild) {
              if (iteratorIndex == this.tabIndex) {
                return MapEntry(
                    iteratorIndex, Offstage(child: aChild, offstage: false));
              } else {
                return MapEntry(
                    iteratorIndex, Offstage(child: aChild, offstage: true));
              }
            })
            .values
            .toList());
  }
}

/* 
  Wrap a body with a Navigation. This way you can easily navigate single screen without loosing root scope (appbar and navigation).
*/
class NavigatorWrapper extends StatelessWidget {
  final Widget child;
  final Key navigatorKey;
  final Map<String, Widget Function(BuildContext)> routes;

  const NavigatorWrapper({Key key, this.navigatorKey, this.child, this.routes})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return routes != null
        ? MaterialApp(
            routes: routes,
            navigatorKey: navigatorKey,
            debugShowCheckedModeBanner: false,
          )
        : MaterialApp(
            home: child,
            navigatorKey: navigatorKey,
            debugShowCheckedModeBanner: false,
          );
  }
}

class SizedDrawer extends StatelessWidget {
  final dynamic headerBackgroundColor;
  final String headerTitle;
  final double width;
  @required
  final List<Widget> children;

  const SizedDrawer(
      {Key key,
      this.headerBackgroundColor,
      this.headerTitle,
      this.width,
      this.children})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: width != null ? width : MediaQuery.of(context).size.width - 100,
        child: Scaffold(
          appBar: AppBar(
            title: Text(headerTitle != null ? headerTitle : ''),
            leading: SizedBox(),
            backgroundColor: headerBackgroundColor != null
                ? headerBackgroundColor
                : Theme.of(context).primaryColor,
          ),
          body: Container(
            padding: EdgeInsets.all(8),
            child: ListView(
              padding: EdgeInsets.zero,
              children: children,
            ),
          ),
        ));
  }
}
