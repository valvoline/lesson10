import 'package:flutter/material.dart';
import 'package:lesson_10/models/contatto.dart';

class ContactCard extends StatelessWidget {
  final Function action;
  final Contatto contatto;
  ContactCard({this.contatto, this.action});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: ListTile(
        onTap: () {
          action();
        },
        title: Text('${contatto.nome}'),
        leading: CircleAvatar(
          backgroundImage: NetworkImage('${contatto.thumb}'),
        ),
        trailing: Icon(Icons.arrow_forward_ios),
      ),
    );
  }
}
