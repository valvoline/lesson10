import UIKit
import Flutter
//import device_token_interceptor

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {
    GeneratedPluginRegistrant.register(with: self)
//    if #available(iOS 10.0, *) {
//      UNUserNotificationCenter.current().delegate = self
//    }
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
//    override func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
//        SwiftDeviceTokenInterceptorPlugin.onToken(deviceToken: deviceToken)
//    }
}

